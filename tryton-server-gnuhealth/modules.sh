#!/bin/bash

for file in ./*; do
	modulename=$(basename ${file})
	if  [[ $modulename == health* ]] ; then
		pip install ${modulename}
	fi
done

